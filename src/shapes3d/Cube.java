package shapes3d;

import shapes2d.Square;

public class Cube extends Square {
    int a;
    public Cube(int a) {
        super(a);
    }

    public Integer area(){
        return 6*a*a;
    }
    public Integer volume(){
        return a*a*a;
    }
}
