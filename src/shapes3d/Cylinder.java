package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {
    int r;
    int h;

    public Cylinder(int r,int h) {
        super(r);
        this.h=h;
    }

    public double area(){
        return (3.14*r*r*2)+(2*3.14*r*h);
    }

    public double volume(){
        return 3.14*r*r*h;
    }
    public String to_string(){
        return "Radius of cylinder="+r+"  "+"height of cylinder="+h;
    }

}
