package shapes2d;

public class Square {
    int a;


    public Square(int a){
        this.a=a;

    }

    public Integer area(){
        return a*a;
    }

    public String toString(){
        String x;
        x=String.valueOf(a);
        return "one side of square="+x;

    }

}
