package shapes2d;

public class Circle {
    int r;

    public Circle(int r){
        this.r=r;
    }

    public double area(){
        return r*r*3.14;
    }

    public String toString(){
        String x;
        x=String.valueOf(r);
        return "Radius of circle="+r;
    }


}
